import {
  Card,
  CardHeader,
  CardContent,
  CardDescription,
  CardTitle,
} from "./ui/card";
import { Badge } from "./ui/badge";

interface Props {
  title: string;
  description: string;
  img: string;
  link?: string;
  year?: string;
}

export function Certificate({ title, description, year, link, img }: Props) {
  return (
    <Card className="flex flex-col overflow-hidden border border-muted p-3">
      <CardHeader className="">
        <div className="space-y-1">
          <CardTitle className="text-base mb-2">
            {link ? (
              <a
                href={link}
                target="_blank"
                className="inline-flex items-center gap-1 hover:underline"
              >
                {title}{" "}
                <span className="size-1 rounded-full bg-green-500"></span>
              </a>
            ) : (
              title
            )}
          </CardTitle>
          <div className="hidden font-mono text-xs underline print:visible">
            {link?.replace("https://", "").replace("www.", "").replace("/", "")}
          </div>
        </div>
      </CardHeader>
      <CardContent className="mt-auto flex">

        <img className="w-[25rem] shadow-xl rounded-lg mb-5" src={img} alt="image description" />

      </CardContent>
      <CardDescription className="font-bold text-sm">
       Achieved {year}
      </CardDescription>
    </Card>
  );
}
