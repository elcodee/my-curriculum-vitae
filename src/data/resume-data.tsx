
import { LinkedInIcon } from "@/components/icons";
import { GitlabIcon, InstagramIcon } from "lucide-react";

export const RESUME_DATA = {
  name: "Rama Aditya",
  initials: "eLCodeee",
  location: "Bekasi Kota Jawa Barat, Indonesia",
  locationLink: "https://www.google.com/maps/place/Kota+Bks,+Jawa+Barat",
  about:
    "Full Stack Engineer focused on building products with extra attention to detail",
  summary:
    "I’m a frontend programer at IT Consultant Company in Indonesia. I dedicated to continuously expanding my knowledge and skills in website development and learning new technologies. I’m able to develop web applications, consume APIs and strive to deliver best code.",
  avatarUrl: "https://media.licdn.com/dms/image/D5603AQFg-2DoECDZqw/profile-displayphoto-shrink_200_200/0/1685278814448?e=1711584000&v=beta&t=WKlxUKq80tj0YsdWYJZiPaDV-yh_dxAX9Vv3rnF132Y",
  personalWebsiteUrl: "https://elcodeee.com/me",
  contact: {
    email: "elcodeee@gmail.com",
    tel: "+6281932709954",
    social: [
      {
        name: "GitLab",
        url: "https://gitlab.com/elcodee",
        icon: GitlabIcon,
      },
      {
        name: "LinkedIn",
        url: "https://www.linkedin.com/in/elcodeee",
        icon: LinkedInIcon,
      },
      {
        name: "Instagram",
        url: "https://instagram.com/elcodeee",
        icon: InstagramIcon,
      },
    ],
  },
  education: [
    {
      school: "PKP 1 Multimedia Vocational School",
      degree: "Graduated with a BNSP Garuda Emas certificate",
      start: "2016",
      end: "2019",
    },
  ],
  work: [
    {
      company: "Klola Indonesia",
      link: "https://klola.id",
      badges: ["Remote"],
      title: "Frontend Developer",
      logo: null,
      start: "2020",
      end: "present",
      description:
        "Implemented new features, worked on improving the way developers ship the code, and more. Technologies: NextJS, TypeScript, Antd",
    },
    {
      company: "Freelance",
      link: "https://elcodeee.com/me",
      badges: ["Remote"],
      title: "Full Stack Developer",
      logo: null,
      start: "2019",
      end: "present",
      description:
        "Created Websites. Built live Panding, E-Commarce, Portofolio, Company Profile, Custom Website, and more application for UMKM from scratch. Technologies: Laravel, NextJS, ReactJS, ExpressJS, TailwindUI, Mysql",
    },
    {
      company: "Level Up Film",
      link: "https://levelupfilms.id/",
      badges: [],
      title: "Adobe Premiere, Adobe Photoshop, Adobe After Effect",
      logo: null,
      start: "2018",
      end: "2018",
      description:
        "Intern as TV Ads & Behind The Scene Editor",
    }
  ],
  skills: [
    "JavaScript",
    "TypeScript",
    "ReactJS/NextJS/ExpressJS",
    "Laravel",
    "Python",
    "NodeJS",
    "Mysql",
    "Supabase",
  ],
  projects: [
    {
      title: "Code Api's",
      techStack: [
        "Side Project",
        "JavaScript",
        "NextJS",
        "Laravel",
        "Innertia",
        "Mysql",
      ],
      description: "A platform to provide artificial intellegent Rest API",
      logo: null,
      link: {
        label: "elcodeee.com",
        href: "https://elcodeee.com",
      },
    },
    {
      title: "PremiPlay",
      techStack: [
        "Side Project",
        "JavaScript",
        "ReactJS",
        "Laravel",
        "Innertia",
        "Mysql",
        "WhatsApp Gateway",
        "Payment Gateway",
      ],
      description:
        "Digital entertaintment store",
      logo: null,
      link: {
        label: "s.id/PremiPlay",
        href: "https://s.id/PremiPlay",
      },
    },
    {
      title: "KelarinT",
      techStack: [
        "Side Project",
        "JavaScript",
        "ReactJS",
        "Laravel",
        "Innertia",
        "Mysql",
        "WhatsApp Gateway",
        "Payment Gateway",
      ],
      description:
        "College assignment writing services",
      logo: null,
      link: {
        label: "s.id/kelarint",
        href: "https://s.id/kelarint",
      },
    },
    {
      title: "eLCodeee",
      techStack: ["Side Project", "Astro", "MDX"],
      description:
        "My personal website. Built with Astro",
      logo: null,
      link: {
        label: "elcodeee.com/me",
        href: "https://elcodeee.com/me",
      },
    },
  ],
  certificate: [
    {
      link :"#",
      title: "Coming Soon...",
      img: "https://flowbite.com/docs/images/examples/image-1@2x.jpg",
      year: "soon",
      description: "A platform to provide artificial intellegent Rest API",
    },
    {
      link :"https://dumbways.id/",
      title: "Fullstack JavaScipt",
      img: "https://github.com/elcodee/certificate/blob/main/rama-dumbways-bootcamp-selection.png?raw=true",
      year: "2021",
      description: "A platform to provide artificial intellegent Rest API",
    },
    {
      link :"https://dumbways.id/",
      title: "Junior Fullstack JavaScipt",
      img: "https://github.com/elcodee/certificate/blob/main/rama-dumbways-preclass.png?raw=true",
      year: "2021",
      description: "A platform to provide artificial intellegent Rest API",
    },
    {
      link :"https://www.hacktiv8.com/full-stack-javascript-immersive?utm_source=google&utm_medium=cpc&utm_campaign=Brand-all&utm_content=hacktiv8&utm_term=responsive-ads&gad_source=1&gclid=Cj0KCQiAh8OtBhCQARIsAIkWb69BN0fRlQ_PbNGcTOWsJPvIxiRaSz2RgWfkxKoQsh-w9IE2C2KdE0MaAqM2EALw_wcB",
      title: "React & React Native Basic",
      img: "https://github.com/elcodee/certificate/blob/main/rama-hacktiv8-rn.png?raw=true",
      year: "2021",
      description: "A platform to provide artificial intellegent Rest API",
    },
    {
      link :"https://codingstudio.id/",
      title: "Fullstack Laravel",
      img: "https://github.com/elcodee/certificate/blob/main/rama-codio.png?raw=true",
      year: "2021",
      description: "A platform to provide artificial intellegent Rest API",
    },
    {
      link :"https://buildwithangga.com/",
      title: "Fullstack Laravel Vue JS",
      img: "https://github.com/elcodee/certificate/blob/main/rama-bwa.png?raw=true",
      year: "2020",
      description: "A platform to provide artificial intellegent Rest API",
    },
    {
      link :"https://www.hacktiv8.com/full-stack-javascript-immersive?utm_source=google&utm_medium=cpc&utm_campaign=Brand-all&utm_content=hacktiv8&utm_term=responsive-ads&gad_source=1&gclid=Cj0KCQiAh8OtBhCQARIsAIkWb69BN0fRlQ_PbNGcTOWsJPvIxiRaSz2RgWfkxKoQsh-w9IE2C2KdE0MaAqM2EALw_wcB",
      title: "Intro to Programming",
      img: "https://github.com/elcodee/certificate/blob/main/rama-hacktiv8-intro.png?raw=true",
      year: "2020",
      description: "A platform to provide artificial intellegent Rest API",
    },
    {
      link :"https://bnsp.go.id/lsp",
      title: "BNSP Multimedia Service Competition",
      img: "https://github.com/elcodee/certificate/blob/main/rama-bnsp.jpeg?raw=true",
      year: "2019",
      description: "A platform to provide artificial intellegent Rest API",
    },
    {
      link :"https://lspr.edu/lxpr/",
      title: "LSPR Radio Announcer Competition",
      img: "https://github.com/elcodee/certificate/blob/main/rama-lspr.png?raw=true",
      year: "2019",
      description: "A platform to provide artificial intellegent Rest API",
    },
  ],
} as const;
